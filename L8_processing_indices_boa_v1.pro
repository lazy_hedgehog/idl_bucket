
;+
; :Description:
;
; :INPUT:
;
; :DEPENDENCE:
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 26 February 2018
;-


PRO L8_processing_indices_BOA_v1

  COMPILE_OPT IDL2
  e = ENVI(/HEADLESS) ; ()

  ;;;;;;;;;;;;;;;;;;;;;
  ;;; TOA
;  in_path = 'K:\personal\kat\TOTRAM\L2\LC082020252016052901T1-SC20180223135201'
;  in_path = 'K:\personal\kat\TOTRAM\L2\LC082030252016060501T1-SC20180223153042'
;  in_path = 'K:\personal\kat\TOTRAM\L2\LC082030252016101101T1-SC20180223135201'
 in_path = 'K:\personal\kat\TOTRAM\L2\LC082030252016112801T1-SC20180223161905'

  ;;; BOA

  ;;;;;;;;;;;;;;;;;;;;;

  print, in_path

  in_b2 = FILE_SEARCH(in_path,'*band1.tif')        
  in_b4 = FILE_SEARCH(in_path,'*band4.tif')    
  in_b5 = FILE_SEARCH(in_path,'*band5.tif')    
  in_b6 = FILE_SEARCH(in_path,'*band6.tif')    
  in_b7 = FILE_SEARCH(in_path,'*band7.tif')    
  
    base_name = FILE_BASENAME(in_b2[0],'band1.tif')

  in_blue =e.OpenRaster(in_b2[0])
  in_blue_fid = ENVIRasterToFID(in_blue)
  in_red =e.OpenRaster(in_b4[0])
  in_red_fid = ENVIRasterToFID(in_red)
  in_nir =e.OpenRaster(in_b5[0])
  in_nir_fid = ENVIRasterToFID(in_nir)
  in_swir1 =e.OpenRaster(in_b6[0])
  in_swir1_fid = ENVIRasterToFID(in_swir1)
  in_swir2 =e.OpenRaster(in_b7[0])
  in_swir2_fid = ENVIRasterToFID(in_swir2)

  ;;; NDVI
  OutFile = in_path + '\' + base_name + 'NDVI.bsq'

  expression = 'FLOAT(B1 - B2) / FLOAT(B1 + B2)'
  math_fid = [in_nir_fid, in_red_fid]
  pos = [0,0]

  ENVI_FILE_QUERY, in_blue_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; NMDI
  OutFile = in_path + '\' + base_name + 'NMDI.bsq'

  expression = 'FLOAT(B1 - (B2 - B3)) / FLOAT(B1 + (B2 - B3))'
  math_fid = [in_nir_fid, in_swir1_fid, in_swir2_fid]
  pos = [0,0,0]

  ENVI_FILE_QUERY, in_blue_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; NDII
  OutFile = in_path + '\' + base_name + 'NDII.bsq'

  expression = 'FLOAT(B1 - B2) / FLOAT(B1 + B2)'
  math_fid = [in_nir_fid, in_swir2_fid]
  pos = [0,0]

  ENVI_FILE_QUERY, in_blue_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; NDWI
  OutFile = in_path + '\' + base_name + 'NDWI.bsq'

  expression = 'FLOAT(B1 - B2) / FLOAT(B1 + B2)'
  math_fid = [in_nir_fid, in_swir1_fid]
  pos = [0,0]

  ENVI_FILE_QUERY, in_blue_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; EVI
  OutFile = in_path + '\' + base_name + 'EVI.bsq'

  expression = '2.5*FLOAT(B1 - B2) / (FLOAT(B1 + 6*B2 - 7.5*B3 +10000))'
  math_fid = [in_nir_fid, in_red_fid, in_blue_fid]
  pos = [0,0,0]

  ENVI_FILE_QUERY, in_blue_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile

  ; Close the ENVI session
  e.Close


END

