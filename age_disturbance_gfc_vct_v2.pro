;+
; :Description:
;
; INPUT:
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 19 July 2017
;-

PRO Age_disturbance_GFC_VCT_v2

  t1 = SYSTIME()

  ;  ;;;catch error
  ;  !error_state.code = 0
  ;  CATCH, error
  ;  if (error ne 0) then begin
  ;    ok = DIALOG_MESSAGE(!error_state.msg, /CANCEL)
  ;    if (STRUPCASE(ok) eq 'CANCEL') then STOP; RETURN
  ;  endif

  ;
        compile_opt STRICTARR
        e=ENVI(/HEADLESS)
  ;      ENVI, /restore_base_save_files
  ;      ENVI_BATCH_INIT


  ;;;;;;;;;;;;;;;;;;;;;

  in_vct_file = 'S:\projects\000044_IdahoForestryGroup\04_DataProducts\L0\NAFD-NEX_Forest_Disturbance_1290'
;  in_gfc = 'D:\DATA\projects\000044_IdahoForestryGroup\04_DataProducts\L0\GFC2000-2015\Hansen_GFC-2015-v1.3_lossyear_subset.bsq'
  in_S2 = 'Z:\projects\000044_IdahoForestryGroup\04_DataProducts\L1\S2\synthetic_mosaic_20161002_20151005x.bsq'

  out_dis_vct = 'Z:\projects\000044_IdahoForestryGroup\04_DataProducts\L1\disturbance\Phase2\NAFD_1986-2010_change_new_big.bsq'
  out_dis_vct_temp = 'Z:\projects\000044_IdahoForestryGroup\04_DataProducts\L1\disturbance\Phase2\NAFD_1986-2010_change_new_big_temp.bsq'



  ;;;;;;;;;;;;;;;;;;;;

  in_list = FILE_SEARCH(in_vct_file,'????.tif')

  fid_list = LONARR(N_ELEMENTS(in_list))

  ENVI_OPEN_FILE, in_list[N_ELEMENTS(in_list)-1], r_fid=in_fid, /NO_REALIZE
  ENVI_FILE_QUERY, in_fid, nb=nb, ns=ns, nl=nl, data_type=data_type, dims=dims

  expression = '(B1 EQ 255)*(2010) + (B1 EQ 200)'
  posmath = [0]
  math_fid = [in_fid]
  value = 'Disturbances'
  ENVI_DOIT, 'MATH_DOIT', fid=math_fid, R_FID=base_fid, pos=posmath, dims=dims, exp=expression, /IN_MEMORY


  FOR i=N_ELEMENTS(in_list)-1, 0, -1 DO BEGIN

    ENVI_OPEN_FILE, in_list[i], r_fid=in_fid, /NO_REALIZE
    ENVI_FILE_QUERY, in_fid, nb=nb, ns=ns, nl=nl, data_type=data_type, dims=dims

    expression = '(B1 EQ 255)*(1986+'+STRTRIM(i,2)+')'
    posmath = [0]
    math_fid = [in_fid]
    value = 'Disturbances'
    ENVI_DOIT, 'MATH_DOIT', fid=math_fid, R_FID=dis_fid, pos=posmath, dims=dims, exp=expression, out_name=out_dis_vct,  /IN_MEMORY


    expression = ' (B1 NE 0)*B1 + (B1 EQ 0)*B2 '
    posmath = [0,0]
    math_fid = [base_fid, dis_fid]
    value = 'Disturbances'
    ENVI_DOIT, 'MATH_DOIT', fid=math_fid, R_FID=base_fid, pos=posmath, dims=dims, exp=expression, out_name=out_dis_vct,  /IN_MEMORY


    ENVI_FILE_MNG, ID=in_fid, /REMOVE
    ENVI_FILE_MNG, ID=dis_fid, /REMOVE

  ENDFOR

  ENVI_OPEN_FILE, in_S2, r_fid=S2_fid, /NO_REALIZE
  ENVI_FILE_QUERY, S2_fid, ns=ns_S2, nl=nl_S2, data_type=data_type_S2, dims=dims_s2
  projection= ENVI_GET_PROJECTION(fid = S2_fid, pixel_size=pixel_size_S2)

  pos=[0,0]
  band_names = ['S2','disturbance']

  dims_stack=LONARR(5,2)
  dims_stack[*,0] = dims_S2
  dims_stack[*,1] = dims

  ENVI_DOIT, 'ENVI_LAYER_STACKING_DOIT', fid=[S2_fid, base_fid], r_fid=layerstack_fid, pos=pos, dims=dims_stack, out_dt=2, $
    out_ps=pixel_size_S2, out_proj=projection, out_bname=band_names, out_name=out_dis_vct_temp , /EXCLUSIVE

  ENVI_DOIT, 'RESIZE_DOIT', DIMS=dims_stack, FID=layerstack_fid, INTERP = 0, $
    OUT_BNAME=['Disturbance'], OUT_NAME=out_dis_vct, POS=[1], R_FID=out_fid, RFACT=[1,1]

  t2 = SYSTIME()                                                    ; end time
  print, 'start time: ' + t1                                        ; print start time
  print, 'end time: ' + t2                                          ; print end time



END