
;+
; :Description: Code processing 2 RapidEye scenes in order to identify windthrow areas
;
; INPUT:  pre-event image
;         post-event image
;         mask of plantation areas
;
; HISTORY: - introduction of additional flag class: plantations established after the windthrow -> 2013 to present
;          - an attempt to distinguish removed trees and trunks on a ground areas
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 16 May 2017
;-

PRO RE_windfall_WA_V3


  t1 = SYSTIME()

  ;;;catch error
  !error_state.code = 0
  CATCH, error
  if (error ne 0) then begin
    ok = DIALOG_MESSAGE(!error_state.msg, /CANCEL)
    if (STRUPCASE(ok) eq 'CANCEL') then STOP; RETURN
  endif


;    compile_opt STRICTARR
;    ENVI, /restore_base_save_files
;    ENVI_BATCH_INIT


  ;;;;;;;;;;;;;;;;;;;;;


  in_mask = 'Z:\WestAustralia\Rapid_Eye\20170516_Plantation_mask_windfall.bsq'
  in_pre = 'Z:\WestAustralia\Rapid_Eye\2012-01-24_RE1_3A_mosaic.bsq'
  in_post = 'Z:\WestAustralia\Rapid_Eye\2014-01-22_RE1_3A_mosaic.bsq'
  in_year = 'Z:\WestAustralia\Rapid_Eye\20170516_EstabYeat.envi'

  pre_mask = 'Z:\WestAustralia\Rapid_Eye\20170519_pre_mask.bsq'
  post_mask = 'Z:\WestAustralia\Rapid_Eye\20170519_post_mask.bsq'
  out_windfall = 'Z:\WestAustralia\Rapid_Eye\20170606_Windfall_mask.bsq'
  out_windfall_thin = 'Z:\WestAustralia\Rapid_Eye\20170606_Windfall_mask_thinning.bsq'
  out_windfal_temp = 'Z:\WestAustralia\Rapid_Eye\20170606_Windfall_mask_temp.bsq'
;  out_shp = 'Z:\WestAustralia\Rapid_Eye\20170519_Windfall_mask.evf'
;  out_shp_thin = 'Z:\WestAustralia\Rapid_Eye\20170519_Windfall_mask_thin_'

  ENVI_OPEN_FILE, in_mask, r_fid = mask_fid, /NO_REALIZE
  ENVI_FILE_QUERY, mask_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=mask_dims

  ENVI_OPEN_FILE, in_year, r_fid = year_fid, /NO_REALIZE
  ENVI_FILE_QUERY, year_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=year_dims


  ENVI_OPEN_FILE, in_pre, r_fid = pre_fid, /NO_REALIZE
  ENVI_FILE_QUERY, pre_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=pre_dims

  ENVI_OPEN_FILE, in_post, r_fid = post_fid, /NO_REALIZE
  ENVI_FILE_QUERY, post_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=post_dims


  ;;; pre-event forest mask

  pos = [1,2,3,4,0] ; [B1 - blue is not used here]
  exp = '((B2 LE 4000 AND B3 LE 3000 AND B4 LE 3500 AND B5 GE 4000)*1)*B6'
  mathband_fid=[pre_fid, pre_fid, pre_fid, pre_fid, mask_fid]

  ;pos = [1,2,3,4,0] ; [B1 - blue is not used here]
  ;exp = '(B2 LE 4000 AND B3 LE 3000 AND B4 LE 3500 AND B5 GE 4000 AND B1 EQ 1)*1'
  ;mathband_fid=[pre_fid, pre_fid, pre_fid, pre_fid, mask_fid]
  b_name = 'Pre-event forest mask'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=pre_dims, exp=exp, out_bname=b_name ,out_name=pre_mask, r_fid=pre_mask_fid; , /IN_MEMORY


  ;;; post-event forest mask
  pos = [1,2,3,4,0] ; [B1 - blue is not used here]
  exp = '((B2 LE 4000 AND B3 LE 3000 AND B4 LE 3500 AND B5 GE 4000)*1)*B6'
  mathband_fid=[post_fid, post_fid, post_fid, post_fid, mask_fid]
  b_name = 'Post-event forest mask'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=post_dims, exp=exp, out_bname=b_name ,out_name=post_mask, r_fid=post_mask_fid; , /IN_MEMORY


  ;;; Windfall mask
  pos=[0,0]
  exp = '(B1 EQ 1 AND B2 EQ 0)*1'
  mathband_fid=[pre_mask_fid, post_mask_fid]
  b_name = 'Windthrow mask'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=mask_dims, exp=exp, out_bname=b_name ,out_name=out_windfall, r_fid=out_mask_fid , /IN_MEMORY


  ;;; morphology based cleaning
  ENVI_DOIT, 'MORPH_DOIT', FID=out_mask_fid, dims=mask_dims, pos=0, METHOD=3, KERNEL=FLTARR(3,3)+1, GRAY=0, out_name=out_windfal_temp, r_fid=out_mask_morph_fid, /IN_MEMORY
  ENVI_DOIT, 'MORPH_DOIT', FID=out_mask_morph_fid, dims=mask_dims, pos=0, METHOD=0, KERNEL=FLTARR(3,3)+1, GRAY=0, out_name=out_windfal_temp, r_fid=out_mask_morph_fid2, /IN_MEMORY
  ENVI_DOIT, 'MORPH_DOIT', FID=out_mask_morph_fid2, dims=mask_dims, pos=0, METHOD=1, KERNEL=FLTARR(3,3)+1, GRAY=0, out_name=out_windfall, r_fid=out_mask_fid, /IN_MEMORY

  ;;; 'clean' with forest mask
  pos=[0,0]
  exp = 'B1*B2'
  mathband_fid = [mask_fid, out_mask_fid]
  b_name = 'Windthrow mask'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=mask_dims, exp=exp, out_bname=b_name ,out_name=out_windfall, r_fid=out_mask1_fid , /IN_MEMORY


  ;;; distingtion between trunks and bare soil windthrow scars
  pos=[0,4]
  exp='B1 + (B1 EQ 1 AND B2 LE 6000)*1 + (B1 EQ 1 AND B2 GT 6000)*2'
  mathband_fid = [out_mask1_fid, post_fid]
  b_name = 'Windthrow mask'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=mask_dims, exp=exp, out_bname=b_name ,out_name=out_windfall, r_fid=out_mask_fid; , /IN_MEMORY


  ;;; screen for potential thinning false alarms
  pos=[0,0]
  exp='B1+((B1 GE 1 AND B2 GE 2000 AND B2 LE 2002) OR (B1 GE 1 AND B2 GE 1992 AND B2 LE 1994))*10 + (B1 GE 2 AND B2 GE 2012)*20'
  mathband_fid = [out_mask_fid, year_fid]
  b_name = 'Windthrow mask, thinning flags'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=mask_dims, exp=exp, out_bname=b_name ,out_name=out_windfall_thin, r_fid=out_mask_thin_fid; , /IN_MEMORY


  ;  ;;; export to evf
  ;  ENVI_DOIT, 'RTV_DOIT', DIMS=mask_dims, FID=out_mask_fid, L_NAME=['disturbance'], OUT_NAME=out_shp, POS=[0], VALUES=[1], IN_MEMORY=LONARR(1)
  ;
  ;  values = ['disturbance', 'thinning']
  ;  ENVI_DOIT, 'RTV_DOIT', DIMS=mask_dims, FID=out_mask_thin_fid, L_NAME=['disturbance', 'thinning'], $
  ;    OUT_NAME=out_shp_thin+string(values)+'.evf', POS=[0], VALUES=[1,2], IN_MEMORY=LONARR(N_ELEMENTS(values))



  t2 = SYSTIME()                                                    ; end time
  print, 'start time: ' + t1                                        ; print start time
  print, 'end time: ' + t2                                          ; print end time



END