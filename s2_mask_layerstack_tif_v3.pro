
;+
; :Description: Code combining Global Forest Change dataset and NACP NAFD Forest Disturbance History from Landsat.
;
; INPUT:  path to the datasts
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 11 August 2017
;-

PRO S2_mask_layerstack_tif_v3


  t1 = SYSTIME()

  ;;;catch error
  !error_state.code = 0
  CATCH, error
  if (error ne 0) then begin
    ok = DIALOG_MESSAGE(!error_state.msg, /CANCEL)
    if (STRUPCASE(ok) eq 'CANCEL') then STOP; RETURN
  endif


  compile_opt IDL2
  e=ENVI(/HEADLESS)

  ;;;;;;;;;;;;;;;;;;;;;

  in_file = 'Z:\projects\000044_IdahoForestryGroup\04_DataProducts\L1\S2\mosaic_20160604.envi'
  ;  in_file = 'Z:\projects\000044_IdahoForestryGroup\04_DataProducts\L1\S2\mosaic_20161208.envi'
  ;  in_file = DIALOG_PICKFILE(/READ, TITLE='Select mosiac file cor indices calculation')

  out_path = 'Z:\projects\000044_IdahoForestryGroup\04_DataProducts\L1\S2_mosaics\'

  base_name = FILE_BASENAME(in_file,'.envi'); .bsq ; .envi
  out = out_path+base_name+'_cl.tif'
  out1 = out_path+base_name+'_cl_t.tif'
  out_mask_t = out_path+base_name+'_mask.bsq

  mask_val = -9999.0

  ENVI_OPEN_FILE, in_file, r_fid=in_fid, /NO_REALIZE
  ENVI_FILE_QUERY, in_fid, ns=ns, nl=nl, data_type=data_type, dims=dims; , bnames=bnames
  projection= ENVI_GET_PROJECTION(fid = in_fid, pixel_size=pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=in_fid)

  bnames = ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']

  pos = [0]
  exp = '(B1 GT 1)*1 '
  mathband_fid=[in_fid]
  b_name = 'mask'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_mask_t, r_fid=mask_fid;, /IN_MEMORY

  ENVI_DOIT, 'ENVI_MASK_APPLY_DOIT', DIMS=dims, FID=in_fid, M_FID=mask_fid, M_POS=[0], OUT_BNAME=bnames, $
    OUT_NAME=out1, POS=[0:9], R_FID=r_fid, VALUE=(-9999);, /IN_MEMORY

  ras = ENVIFIDToRaster(r_fid)
  IF (ras.metadata.HasTag ('data ignore value')) THEN BEGIN
    ras.metadata.UpdateItem, 'data ignore value', -9999.0
  ENDIF ELSE BEGIN
    ras.metadata.AddItem, 'data ignore value', -9999.0
  ENDELSE
  ras.WriteMetadata


  ;    ras = ENVIFIDToRaster(r_fid)
  e.ExportRaster, ras, out, 'TIFF'
  ras.close

  ras = ENVIFIDToRaster(mask_fid)
  ras.close

  FILE_DELETE, out_mask_t
  FILE_DELETE, out1


  t2 = SYSTIME()                                                    ; end time
  print, 'start time: ' + t1                                        ; print start time
  print, 'end time: ' + t2                                          ; print end time



END