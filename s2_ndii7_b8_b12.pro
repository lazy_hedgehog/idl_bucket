
;FUNCTION S2_calculate_ndii7_B8_B12, in_dir, layerstack_fid, ndii7_B8_B12_fid  ;out_ndii7_B8_B12 ;,

PRO S2_ndii7_B8_B12

  ;;;Katarzyna_Ewa_Lewinska

  ;;; FUNCTION CALCULATES NDII7 USING SENTINEL-2 BAND 8 AND 11. INPUT FILE IS A LAYERSTACK OF SENTINEL-2 BANDS: B2, B3, B4, B5, B6, B7, B8, B8A, B11 and B12
  ;;; (IN THIS ORDER) DERIVED FOR EXAMPLE USING THE S2-layerstack_10m.pro FUNCTION


    ly_stack = 'Z:\S2_S\tiles\T33VWJ\T33VWJ_20170602_layerstack.bsq'

;    in_dir = 'Z:\S2_S\tiles\T33VWJ'
;  path = in_dir+'indices'
;
;  test_dir = FILE_TEST(path, /DIRECTORY)
;  IF test_dir EQ 0 THEN FILE_MKDIR, path

  out_ndii7_B8_B12 = FILE_DIRNAME(ly_stack)+'\'+ FILE_BASENAME(ly_stack,'layerstack.bsq')+ 'ndii7_B8_B12.bsq'

  print, 'calculate NDII7 (band 8 and 12)'


  COMPILE_OPT STRICTARR
      ENVI, /restore_base_save_files
  ENVI_BATCH_INIT

  ;;;OPEN INPUT FILE
  ENVI_OPEN_FILE, ly_stack, r_fid = layerstack_fid, /NO_REALIZE
  ENVI_FILE_QUERY, layerstack_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims, bnames=bnames
  projection = ENVI_GET_PROJECTION(fid = layerstack_fid, pixel_size = pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=layerstack_fid)

  pos = [6,9]; B8 and B11 respectively
  exp = 'FLOAT(FLOAT(b1)-FLOAT(b2))/FLOAT(FLOAT(b1)+FLOAT(b2))'
  mathband_fid=[layerstack_fid, layerstack_fid]

  b_name = 'NDII7: (B8-B12)/(B8+B12)'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_ndii7_B8_B12, r_fid=ndii7_B8_B12_fid;, /IN_MEMORY

  ;  RETURN, out_ndii7_B8_B12
;  RETURN, ndii7_B8_B12_fid


END