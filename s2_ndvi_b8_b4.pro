
;+
; :Description:
;    Function calculates NDVI using Sentinel-2 band 8 and 4. input file is a layerstack of S-2 bands in the following order:
;    B2, B3, B4, B5, B6, B7, B8, B8A, B11 and B12.
;
; :Params:
;    in_dir - INPUT dir directory passed from the s2_processing.pro
;    layerstack_fid - INPUT fid passed from the S2_layerstack_10.pro through the s2_processing.pro
;    ndvi_fid - OUTPUT fid of NDVI dataset
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :YEAR: 2016
;-


;FUNCTION S2_calculate_ndvi, in_dir, layerstack_fid, ndvi_fid

PRO S2_ndvi_B8_B4

 ly_stack = 'Z:\S2_S\tiles\T33VWJ\T33VWJ_20170602_layerstack.bsq'

;    in_dir = 'Z:\S2_S\tiles\T33VWJ'
;  path = in_dir+'indices'
;
;  test_dir = FILE_TEST(path, /DIRECTORY)
;  IF test_dir EQ 0 THEN FILE_MKDIR, path

  out_ndvi = FILE_DIRNAME(ly_stack)+'\'+ FILE_BASENAME(ly_stack,'layerstack.bsq')+ 'ndvi.bsq'

  print, 'calculate NDVI'

  COMPILE_OPT STRICTARR
  ENVI, /restore_base_save_files
  ENVI_BATCH_INIT

  ;;;OPEN INPUT FILE
  ENVI_OPEN_FILE, ly_stack, r_fid = layerstack_fid, /NO_REALIZE
  ENVI_FILE_QUERY, layerstack_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims, bnames=bnames
  projection = ENVI_GET_PROJECTION(fid = layerstack_fid, pixel_size = pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=layerstack_fid)

  pos = [6,2]; B8 and B4 respectively
  exp = 'FLOAT(FLOAT(b1)-FLOAT(b2))/FLOAT(FLOAT(b1)+FLOAT(b2))'
  mathband_fid=[layerstack_fid, layerstack_fid]

  b_name = 'NDVI: (B8-B4)/(B8+B4)'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_ndvi, r_fid=ndvi_fid;, /IN_MEMORY

;  RETURN, ndvi_fid

END