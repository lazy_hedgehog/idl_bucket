

; e=ENVI(/HEADLESS)

in_b11 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B11.tif' 
in_b12 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B12.tif'
in_b2 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B02.tif' 
in_b3 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B03.tif' 
in_b4 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B04.tif' 
in_b5 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B05.tif' 
in_b6 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B06.tif' 
in_b7 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B07.tif' 
in_b8 = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B08.tif' 
in_b8A = 'X:\projects\000045_WAPRES\04_DataProducts\L0\S2\T50HMH\S270203B8A.tif'


out_file = 'Y:\000045_WAPRES\04_DataProducts\L0\S2\S170203_layerstack.bsq'

band2=e.OpenRaster(in_b2) ; Band 2

      ; Spatial reference of Bans 2 [10m]
      spatialRef = band2.SPATIALREF
      nCols = band2.NCOLUMNS
      nRows = band2.NROWS
      coordSysCode = spatialRef.COORD_SYS_CODE
      coordSys = ENVICoordSys(COORD_SYS_CODE=coordSysCode)
      pixelSize = spatialRef.PIXEL_SIZE
      tiePointMap = spatialRef.TIE_POINT_MAP
      tiePointPixel = spatialRef.TIE_POINT_PIXEL

      ; Create a grid definition
      Grid = ENVIGridDefinition(coordSys, PIXEL_SIZE=pixelSize, TIE_POINT_MAP=tiePointMap, TIE_POINT_PIXEL=tiePointPixel, NCOLUMNS=nCols, NROWS=nRows)

      ;get all the bands
      band3 = e.OpenRaster(in_b3)
      band4 = e.OpenRaster(in_b4)
      band5 = e.OpenRaster(in_b5)
      band6 = e.OpenRaster(in_b6)
      band7 = e.OpenRaster(in_b7)
      band8 = e.OpenRaster(in_b8)
      band8A = e.OpenRaster(in_b8A)
      band11 = e.OpenRaster(in_b11)
      band12 = e.OpenRaster(in_b12)


      ; Create a layer stack
      layerStack = ENVILayerStackRaster([band2, band3, band4, band5, band6, band7, band8, band8A, band11, band12], GRID_DEFINITION=Grid)

      newFile = e.GetTemporaryFilename()
      layerStack.Export, out_file, 'ENVI'
      
      layerStack = e.OpenRaster(out_file)

      layerStack.Metadata.UpdateItem, 'BAND NAMES', ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12']
      layerStack.WriteMetadata