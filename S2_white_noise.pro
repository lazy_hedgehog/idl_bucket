
;+
; :Description: the code identifies all locations for which pixels' values correspond to a givel n-dimensional value vector.
;               values' fit is +/- a and b, where both parameters are manually adjusted by an operator.
;
; :INPUT:
;
; :DEPENDENCE:;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 21 March 2018
;-


PRO S2_White_Noise


  ;;;;;;;;;;;;;;;;;;;;;

;  in_file = 'K:\personal\kat\FIA_white_noise\oryginal\11TNN_20170729T185816_A_STACK_2kx2k.bsq'
;  in_file = 'K:\personal\kat\FIA_white_noise\oryginal\11TNN_20170805T185034_A_STACK_2kx2k.bsq'
  in_file = 'K:\personal\kat\FIA_white_noise\oryginal\11TNN_20170825T184613_A_STACK_2kx2k.bsq'
  
  out_folder = 'K:\personal\kat\FIA_white_noise\WhiteNoise'

  wn = 6  ;;; White Noise factor for multiplication
  
  ;;;;;;;;;;;;;;;;;;;;;

  ;  COMPILE_OPT STRICTARR
  ;    ENVI, /restore_base_save_files
  ;  ENVI_BATCH_INIT


    ;;; OPEN LAYERSTACK 
    ENVI_OPEN_FILE, in_file, r_fid = in_fid1, /NO_REALIZE
    ENVI_FILE_QUERY, in_fid1, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dimsin, bnames=bnames
    map_info = ENVI_GET_MAP_INFO(fid=in_fid1)
    projection = ENVI_GET_PROJECTION(fid=in_fid1, pixel_size=pixel_size)

  out_fid_arr = INTARR(nb)

  FOR i=0, nb-1 DO BEGIN

    exp = 'B1 + RANDOMN(seed,'+STRING(ns)+','+string(nl)+')*'+STRING(wn)

    pos=i
    mathband_fid=in_fid1
    b_name = 'WN'

    out_file = out_folder + '\' + FILE_BASENAME(in_file,'.bsq') + '_WN_' + '.bsq'

    ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dimsin, exp=exp, out_bname=b_name ,out_name=out_file, r_fid=out_fid, /IN_MEMORY

    out_fid_arr[i]=out_fid

  ENDFOR

  pos2 = LONARR(N_ELEMENTS(out_fid_arr))*0
  dims3 = LONARR(5,N_ELEMENTS(out_fid_arr))
  FOR m=0, N_ELEMENTS(out_fid_arr)-1 DO dims3[*,m]=dimsin

  out_file = out_folder + '\' + FILE_BASENAME(in_file,'.bsq') + '_WN.bsq'

  ENVI_DOIT, 'ENVI_LAYER_STACKING_DOIT', fid=out_fid_arr, r_fid=out_fid, pos=pos2, dims=dims3, $
    out_dt=4, out_ps=pixel_size, out_proj=projection, out_bname = bnames, out_name=out_file


END

