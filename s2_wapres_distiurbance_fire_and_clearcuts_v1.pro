
;+
; :Description: Disturbance detection for a dense S2 time series WAPRES. 
;               Discrimination between fire and clearfelling disturbances.
;
; INPUT:  paths to the datasts
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 20 September 2017
;-

PRO S2_WAPRES_distiurbance_fire_and_clearcuts_v1


  t1 = SYSTIME()

  ;    ;;;catch error
  ;    !error_state.code = 0
  ;    CATCH, error
  ;    if (error ne 0) then begin
  ;      ok = DIALOG_MESSAGE(!error_state.msg, /CANCEL)
  ;      if (STRUPCASE(ok) eq 'CANCEL') then STOP; RETURN
  ;    endif7

  COMPILE_OPT IDL2
  e=ENVI(/HEADLESS)
  ;  ENVI, /restore_base_save_files
  ;  ENVI_BATCH_INIT

  ;;;;;;;;;;;;;;;;;;;;;

  in_path_l = 'Y:\000045_WAPRES\04_DataProducts\L0\S2\'
  in_dist = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\Disturbance_class_sieve8x10.bsq'
  
  out_path = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\'

  ;;;;;;;;;;;;;;;;;;;;;


    out_dist_type = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\Disturbance_type2.bsq'


  in_list = FILE_SEARCH(in_path_l,'*_layerstack.bsq')


  ENVI_OPEN_FILE, in_dist, r_fid=in_fid_d, /NO_REALIZE
  ENVI_FILE_QUERY, in_fid_d, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims_, bnames=bnames, num_classes=num_classes
  projection= ENVI_GET_PROJECTION(fid = in_fid_d, pixel_size=pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=in_fid_d)

 
  out_t_fid_list = INDGEN(N_ELEMENTS(in_list)-1)

 IF N_ELEMENTS(in_list) NE nb+1 THEN STOP

  FOR i=0, N_ELEMENTS(in_list)-3 DO BEGIN

    ENVI_OPEN_FILE, in_list[i], r_fid=in_fid1, /NO_REALIZE
    ENVI_OPEN_FILE, in_list[i+1], r_fid=in_fid2, /NO_REALIZE
    ENVI_OPEN_FILE, in_list[i+2], r_fid=in_fid3, /NO_REALIZE


      ; disturbance type
      pos = [i, 3, 4, 5, 6, 3, 4, 5, 6, 8, 0, 3, 4, 5, 6, 8, 0, 2, 2] ; disturbance mask
      exp = '(b1 eq 1 AND (((B2 + B3 + B4 + B5) GT (B6 + B7 + B8 + B9) AND (B10-B11) LT 2000 AND (B9-B7) LT 400) OR ((B2 + B3 + B4 + B5) GT (B12 + B13 + B14 + B15 ) AND (B16-B17) LT 2000 AND (B15-B13) LT 400) AND (B18 LT 1120 OR B19 LT 1120)))*1'   ; 1 FIRE, 2 CC

;      exp = '(b1 eq 1 AND (((B2 + B3 + B4 + B5) GT (B6 + B7 + B8 + B9) AND (B10-B11) LT 2000 AND (B9-B7) LT 400) OR ((B2 + B3 + B4 + B5) GT (B12 + B13 + B14 + B15 ) AND (B16-B17) LT 2000 AND (B15-B13) LT 400) AND (B18 LT 800 OR B19 LT 800)))*1'   ; 1 FIRE, 2 CC

;      exp = '(b1 eq 1 AND (((B2 + B3 + B4 + B5) GT (B6 + B7 + B8 + B9) AND (B10-B11) LT 2000) OR ((B2 + B3 + B4 + B5) GT (B12 + B13 + B14 + B15 ) AND (B16-B17) LT 2000) AND (B18 LT 800 OR B19 LT 800)))*1'   ; 1 FIRE, 2 CC
;       exp = '(b1 eq 1 AND (((B2 + B3 + B4 + B5) GT (B6 + B7 + B8 + B9) AND (B10-B11) LT 2000))OR (((B2 + B3 + B4 + B5) GT (B12 + B13 + B14 + B15 ) AND (B16-B17) LT 2000)) AND (B18 LT 800 OR B19 LT 800))*1 '  ; 1 FIRE, 2 CC
;       exp = '(b1 eq 1 AND B2 GT b3 AND B4 LT B5)*1 '  ; 1 FIRE, 2 CC
;      exp = '(b1 eq 1 AND B2 GT b3 AND B4 LT B5)*1 + (b1 eq 1 AND B2 LT b3 AND B4 GE B5)*2'  ; 1 FIRE, 2 CC
      mathband_fid = [in_fid_d, in_fid1, in_fid1, in_fid1, in_fid1, in_fid2, in_fid2, in_fid2, in_fid2, in_fid2, in_fid2, $ 
                      in_fid3, in_fid3, in_fid3, in_fid3, in_fid3, in_fid3, in_fid2, in_fid3]
    
      dims = INTARR(5,N_ELEMENTS(pos))
      FOR l=0, N_ELEMENTS(pos)-1 DO dims[*,l]=dims_
    
      ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_t_name, r_fid=out_t_fid, /IN_MEMORY
    
      ENVI_DOIT, 'MORPH_DOIT', FID=out_t_fid, dims=dims_, pos=0, METHOD=3, KERNEL=FLTARR(3,3)+1, GRAY=0, r_fid=out_t_m_fid, /IN_MEMORY

    
      out_t_fid_list[i] = out_t_m_fid
   
   ENVI_FILE_MNG, ID=out_t_fid, /REMOVE

  ENDFOR
  
  
  FOR j= N_ELEMENTS(in_list)-2, N_ELEMENTS(in_list)-1 DO BEGIN

    ENVI_OPEN_FILE, in_list[i], r_fid=in_fid1, /NO_REALIZE
    ENVI_OPEN_FILE, in_list[i+1], r_fid=in_fid2, /NO_REALIZE


    ; disturbance type
    pos = [i, 3, 4, 5, 6, 3, 4, 5, 6, 8, 0, 2] ; disturbance mask
    exp = '(b1 eq 1 AND (B2 + B3 + B4 + B5) GT (B6 + B7 + B8 + B9) AND (B10-B11) LT 2000 AND (B9-B7) LT 380 AND B12 LT 800)*1 '  ; 1 FIRE, 2 CC
    ;       exp = '(b1 eq 1 AND B2 GT b3 AND B4 LT B5)*1 '  ; 1 FIRE, 2 CC
    ;      exp = '(b1 eq 1 AND B2 GT b3 AND B4 LT B5)*1 + (b1 eq 1 AND B2 LT b3 AND B4 GE B5)*2'  ; 1 FIRE, 2 CC
    mathband_fid = [in_fid_d, in_fid1, in_fid1, in_fid1, in_fid1, in_fid2, in_fid2, in_fid2, in_fid2, in_fid2, in_fid2, in_fid2]

    dims = INTARR(5,N_ELEMENTS(pos))
    FOR l=0, N_ELEMENTS(pos)-1 DO dims[*,l]=dims_

    ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_t_name, r_fid=out_t_fid, /IN_MEMORY

    ENVI_DOIT, 'MORPH_DOIT', FID=out_t_fid, dims=dims_, pos=0, METHOD=3, KERNEL=FLTARR(3,3)+1, GRAY=0, r_fid=out_t_m_fid, /IN_MEMORY

    out_t_fid_list[i] = out_t_m_fid
    
    ENVI_FILE_MNG, ID=out_t_fid, /REMOVE


  ENDFOR
  
  
  
  pos = INTARR(N_ELEMENTS(out_t_fid_list))
  
  dims = INTARR(5,N_ELEMENTS(pos))
  FOR l=0, N_ELEMENTS(out_t_fid_list)-1 DO dims[*,l]=dims_


   ENVI_DOIT, 'ENVI_LAYER_STACKING_DOIT', fid=out_t_fid_list, r_fid=ls_d_fid, pos=pos, dims=dims, out_dt=1, $
     out_ps=pixel_size, out_proj=projection, out_bname=out_d_names_list ,out_name=out_dist_type, /EXCLUSIVE



  e.close

  t2 = SYSTIME()                                                    ; end time
  print, 'start time: ' + t1                                        ; print start time
  print, 'end time: ' + t2                                          ; print end time



END