

PRO wapres_NBRI

COMPILE_OPT IDL2
e=ENVI(/HEADLESS)

;;;;;;;;;;;;;;;;;;;;;

in_path = 'Y:\000045_WAPRES\04_DataProducts\L0\S2\'

out_path = 'Y:\000045_WAPRES\04_DataProducts\L1\S2\NBRI\'


;;;;;;;;;;;;;;;;;;;;;

in_list = FILE_SEARCH(in_path,'*_layerstack.bsq')  


  FOR i=0, N_ELEMENTS(in_list)-1 DO BEGIN
    
    ENVI_OPEN_FILE, in_list[i], r_fid=in_fid, /NO_REALIZE
    ENVI_FILE_QUERY, in_fid, ns=ns, nl=nl, data_type=data_type, dims=dims
    projection= ENVI_GET_PROJECTION(fid = in_fid, pixel_size=pixel_size)
    map_info = ENVI_GET_MAP_INFO(fid=in_fid)
    
    
    ; NBRI
    base_name = FILE_BASENAME(in_list[i],'_layerstack.bsq')
    out = out_path+base_name+'_NBRI.bsq'
    
    
    pos = [6,9]; B8 and B12 respectively
    exp = 'FLOAT(B1 NE 0)*(FLOAT(FLOAT(b2)-FLOAT(b1))/FLOAT(FLOAT(b2)+FLOAT(b1)))'
    mathband_fid=[in_fid, in_fid]
    
    b_name = 'NBRI'
    
    ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out, r_fid=out_fid;, /IN_MEMORY
    
  ENDFOR
  
  
END