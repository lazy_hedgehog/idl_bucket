
;+
; :Description: Disturbance detection for a dense S2 time series WAPRES. Sieve preliminary disturbance layer (a layerstack converted to envi classification file
;               with one dummy class (ENVI requires at least 2 classess + Unclassified). 
;
; INPUT:  paths to the datasts
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 20 September 2017
;-

PRO S2_WAPRES_distiurbance_sieve_v1


  t1 = SYSTIME()

  ;    ;;;catch error
  ;    !error_state.code = 0
  ;    CATCH, error
  ;    if (error ne 0) then begin
  ;      ok = DIALOG_MESSAGE(!error_state.msg, /CANCEL)
  ;      if (STRUPCASE(ok) eq 'CANCEL') then STOP; RETURN
  ;    endif7

  COMPILE_OPT IDL2
  e=ENVI(/HEADLESS)
  ;  ENVI, /restore_base_save_files
  ;  ENVI_BATCH_INIT

  ;;;;;;;;;;;;;;;;;;;;;

  in_path = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\'
;  in_dist = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\Disturbance_class.bsq'
  in_dist = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\Disturbance_Fire_class.bsq'
    

  ;;;;;;;;;;;;;;;;;;;;;

  out_dist = 'Y:\000045_WAPRES\04_DataProducts\L2\Dissturbances\Disturbance_Fire_class_sieve8x10.bsq'

  ENVI_OPEN_FILE, in_dist, r_fid=in_fid1, /NO_REALIZE
  ENVI_FILE_QUERY, in_fid1, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims_, bnames=bnames, num_classes=num_classes
  projection= ENVI_GET_PROJECTION(fid = in_fid1, pixel_size=pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=in_fid1)
  
  
  out_fid_list = INDGEN(nb)

  FOR i=0, nb-1 DO BEGIN

    ENVI_DOIT, 'CLASS_CS_DOIT', DIMS=dims_, FID=in_fid1, METHOD=1, OUT_BNAME=bnames[i], POS=i, R_FID=out_sieve_fid, SIEVE_MIN=10, /IN_MEMORY, /EIGHT

    out_fid_list[i] = out_sieve_fid
  
  ENDFOR
  
  
  pos = INTARR(nb)
  dims = INTARR(5,nb)
  FOR l=0, nb-1 DO dims[*,l]=dims_

  ENVI_DOIT, 'ENVI_LAYER_STACKING_DOIT', fid=out_fid_list, r_fid=ls_d_fid, pos=pos, dims=dims, out_dt=1, $
    out_ps=pixel_size, out_proj=projection, out_bname=bnames ,out_name=out_dist, /EXCLUSIVE

  

  e.close

  t2 = SYSTIME()                                                    ; end time
  print, 'start time: ' + t1                                        ; print start time
  print, 'end time: ' + t2                                          ; print end time



END