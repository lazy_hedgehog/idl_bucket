
PRO S2_ndwi_B8_B11

;FUNCTION S2_calculate_ndwi_B8_B11, in_dir, layerstack_fid, ndwi_B8_B11_fid ; out_ndwi_B8_B11 ;,

  ;;;Katarzyna_Ewa_Lewinska

  ;;; FUNCTION CALCULATES NDWI USING SENTINEL-2 BAND 8 AND 11.
  ;;; INPUT FILE IS A LAYERSTACK OF SENTINEL-2 BANDS: B2, B3, B4, B5, B6, B7, B8, B8A, B11 and B12
  ;;; (IN THIS ORDER) DERIVED FOR EXAMPLE USING THE S2-layerstack_10m.pro FUNCTION
  ;;; !!! SINCE THE NDWI USES 1.24µm bandwith, it is impossible to calculate the exact NDWI using S2 data. !!!


  ;  ly_stack = 'Z:\Kasia\01_Data\S2A_USER_PRD_MSIL2A_PDMC_20160506T003405_R112_V20160505T172037_20160505T172037.SAFE\S2A_USER_MTD_SAFL2A_PDMC_10m\S2_10m_layerstack.bsq'

  ;  in_dir = 'Z:\Kasia\01_Data\S2A_USER_PRD_MSIL2A_PDMC_20160506T003405_R112_V20160505T172037_20160505T172037.SAFE\S2A_USER_MTD_SAFL2A_PDMC_10m\'
 
  ly_stack = 'Z:\S2_S\tiles\T33VWJ\T33VWJ_20170602_layerstack.bsq'
  
  out_ndwi_B8_B11 = FILE_DIRNAME(ly_stack)+'\'+ FILE_BASENAME(ly_stack,'layerstack.bsq')+ 'ndwi_B8_B11.bsq'

  print, 'calculate NDWI (band 8 and 11)'


  COMPILE_OPT STRICTARR
      ENVI, /restore_base_save_files
  ENVI_BATCH_INIT

  ;;;OPEN INPUT FILE
    ENVI_OPEN_FILE, ly_stack, r_fid = layerstack_fid, /NO_REALIZE
  ENVI_FILE_QUERY, layerstack_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims, bnames=bnames
  projection = ENVI_GET_PROJECTION(fid = layerstack_fid, pixel_size = pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=layerstack_fid)

  pos = [6,8]; B8 and B11 respectively
  exp = 'FLOAT(FLOAT(b1)-FLOAT(b2))/FLOAT(FLOAT(b1)+FLOAT(b2))'
  mathband_fid=[layerstack_fid, layerstack_fid]

  b_name = 'NDWI: (B8-B11)/(B8+B11)'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_ndwi_B8_B11, r_fid=ndwi_B8_B11_fid;, /IN_MEMORY

;  RETURN, ndwi_B8_B11_fid ; out_ndwi_B8_B11


END