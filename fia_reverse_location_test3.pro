
;+
; :Description: the code identifies all locations for which pixels' values correspond to a givel n-dimensional value vector.
;               values' fit is +/- a and b, where both parameters are manually adjusted by an operator.
;
; :INPUT:
;
; :DEPENDENCE:;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 21 March 2018
;-


PRO FIA_reverse_location_test3


  ;;;;;;;;;;;;;;;;;;;;;

  in_csv1 = 'K:\personal\kat\FIA_white_noise\Reverse\11TNN_20170729T185816_point_intersection.csv'
  in_csv2 = 'K:\personal\kat\FIA_white_noise\Reverse\11TNN_20170805T185034_point_intersection.csv'
  in_csv3 = 'K:\personal\kat\FIA_white_noise\Reverse\11TNN_20170825T184613_point_intersection.csv'
 
  in_folder = 'K:\personal\kat\FIA_white_noise\Reverse\Filtered'
;  in_folder = 'K:\personal\kat\FIA_white_noise\WhiteNoise\Filtered'
  
  in_list1 = FILE_SEARCH(in_folder,'11TNN_20170729T185816*.bsq')
  in_list2 = FILE_SEARCH(in_folder,'11TNN_20170805T185034*.bsq')
  in_list3 = FILE_SEARCH(in_folder,'11TNN_20170825T184613*.bsq')

  out_folder = 'K:\personal\kat\FIA_white_noise\Reverse\Locations\multi-temp'
;  out_folder = 'K:\personal\kat\FIA_white_noise\WhiteNoise\Locations\multi-temp'

  a = 50; 60  ; 20
  b = 100; 120  ; 50
  point_no = 84

  ;;;;;;;;;;;;;;;;;;;;;

  ;;;DATE 1
  ref_points = READ_CSV(in_csv1, HEADER=header)

  point_id = FIX(ref_points.FIELD02)
  band2 = ref_points.FIELD03
  band3 = ref_points.FIELD04
  band4 = ref_points.FIELD05
  band5 = ref_points.FIELD06
  band6 = ref_points.FIELD07
  band7 = ref_points.FIELD08
  band8 = ref_points.FIELD09
  band8A = ref_points.FIELD10
  band11 = ref_points.FIELD11
  band12 = ref_points.FIELD12

  ref_points1 = TRANSPOSE([[FIX(point_id)],[FLOAT(band2)],[FLOAT(band3)],[FLOAT(band4)],[FLOAT(band5)],$
    [FLOAT(band6)],[FLOAT(band7)],[FLOAT(band8)],[FLOAT(band8A)],[FLOAT(band11)],[FLOAT(band12)]])

  ;;; DATE 2
  ref_points = READ_CSV(in_csv2, HEADER=header)

  point_id = FIX(ref_points.FIELD02)
  band2 = ref_points.FIELD03
  band3 = ref_points.FIELD04
  band4 = ref_points.FIELD05
  band5 = ref_points.FIELD06
  band6 = ref_points.FIELD07
  band7 = ref_points.FIELD08
  band8 = ref_points.FIELD09
  band8A = ref_points.FIELD10
  band11 = ref_points.FIELD11
  band12 = ref_points.FIELD12

  ref_points2 = TRANSPOSE([[FIX(point_id)],[FLOAT(band2)],[FLOAT(band3)],[FLOAT(band4)],[FLOAT(band5)],$
    [FLOAT(band6)],[FLOAT(band7)],[FLOAT(band8)],[FLOAT(band8A)],[FLOAT(band11)],[FLOAT(band12)]])

  ;;; DATE 3
  ref_points = READ_CSV(in_csv3, HEADER=header)

  point_id = FIX(ref_points.FIELD02)
  band2 = ref_points.FIELD03
  band3 = ref_points.FIELD04
  band4 = ref_points.FIELD05
  band5 = ref_points.FIELD06
  band6 = ref_points.FIELD07
  band7 = ref_points.FIELD08
  band8 = ref_points.FIELD09
  band8A = ref_points.FIELD10
  band11 = ref_points.FIELD11
  band12 = ref_points.FIELD12

  ref_points3 = TRANSPOSE([[FIX(point_id)],[FLOAT(band2)],[FLOAT(band3)],[FLOAT(band4)],[FLOAT(band5)],$
    [FLOAT(band6)],[FLOAT(band7)],[FLOAT(band8)],[FLOAT(band8A)],[FLOAT(band11)],[FLOAT(band12)]])

  point1 = ref_points1[1:-1,point_no]
  point2 = ref_points2[1:-1,point_no]
  point3 = ref_points3[1:-1,point_no]


;  COMPILE_OPT STRICTARR
;    ENVI, /restore_base_save_files
;  ENVI_BATCH_INIT

  out_fid_arr = INTARR(N_ELEMENTS(in_list1))

  FOR i=0, N_ELEMENTS(in_list1)-1 DO BEGIN

    ;;; OPEN LAYERSTACK FOR DATE 1
    ENVI_OPEN_FILE, in_list1[i], r_fid = in_fid1, /NO_REALIZE
    ENVI_FILE_QUERY, in_fid1, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dimsin, bnames=bnames
    map_info = ENVI_GET_MAP_INFO(fid=in_fid1)
    projection = ENVI_GET_PROJECTION(fid=in_fid1, pixel_size=pixel_size)
    
    ;;; OPEN LAYERSTACK FOR DATE 2
    ENVI_OPEN_FILE, in_list2[i], r_fid = in_fid2, /NO_REALIZE
    ENVI_FILE_QUERY, in_fid2, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dimsin, bnames=bnames
;    map_info = ENVI_GET_MAP_INFO(fid=in_fid2)
;    projection = ENVI_GET_PROJECTION(fid=in_fid2, pixel_size=pixel_size)

    ;;; OPEN LAYERSTACK FOR DATE 3
    ENVI_OPEN_FILE, in_list3[i], r_fid = in_fid3, /NO_REALIZE
    ENVI_FILE_QUERY, in_fid3, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dimsin, bnames=bnames
;    map_info = ENVI_GET_MAP_INFO(fid=in_fid3)
;    projection = ENVI_GET_PROJECTION(fid=in_fid3, pixel_size=pixel_size)
    
    

    exp = '(B1 GE'+ string(point1[0]-a)+ ' and B1 LE' + string(point1[0]+a)+ $
      ' AND B2 GE'+ string(point1[1]-a)+ ' and B2 LE' + string(point1[1]+a)+ $
      ' AND B3 GE'+ string(point1[2]-a)+ ' and B3 LE' + string(point1[2]+a)+ $
      ' AND B4 GE'+ string(point1[3]-a)+ ' and B4 LE' + string(point1[3]+a)+ $
      ' AND B5 GE'+ string(point1[4]-a)+ ' and B5 LE' + string(point1[4]+a)+ $
      ' AND B6 GE'+ string(point1[5]-a)+ ' and B6 LE' + string(point1[5]+a)+ $
      ' AND B7 GE'+ string(point1[6]-b)+ ' and B7 LE' + string(point1[6]+b)+ $
      ' AND B8 GE'+ string(point1[7]-b)+ ' and B8 LE' + string(point1[7]+b)+ $
      ' AND B9 GE'+ string(point1[8]-a)+ ' and B9 LE' + string(point1[8]+a)+ $
      ' AND B10 GE'+ string(point1[9]-a)+ ' and B10 LE' + string(point1[9]+a)+ $
      ' AND B11 GE'+ string(point2[0]-a)+ ' and B11 LE' + string(point2[0]+a)+ $   ; Date #2
      ' AND B12 GE'+ string(point2[1]-a)+ ' and B12 LE' + string(point2[1]+a)+ $
      ' AND B13 GE'+ string(point2[2]-a)+ ' and B13 LE' + string(point2[2]+a)+ $
      ' AND B14 GE'+ string(point2[3]-a)+ ' and B14 LE' + string(point2[3]+a)+ $
      ' AND B15 GE'+ string(point2[4]-a)+ ' and B15 LE' + string(point2[4]+a)+ $
      ' AND B16 GE'+ string(point2[5]-a)+ ' and B16 LE' + string(point2[5]+a)+ $
      ' AND B17 GE'+ string(point2[6]-b)+ ' and B17 LE' + string(point2[6]+b)+ $
      ' AND B18 GE'+ string(point2[7]-b)+ ' and B18 LE' + string(point2[7]+b)+ $
      ' AND B19 GE'+ string(point2[8]-a)+ ' and B19 LE' + string(point2[8]+a)+ $
      ' AND B20 GE'+ string(point2[9]-a)+ ' and B20 LE' + string(point2[9]+a)+ $
      ' AND B21 GE'+ string(point3[0]-a)+ ' and B21 LE' + string(point3[0]+a)+ $   ; Date #3
      ' AND B22 GE'+ string(point3[1]-a)+ ' and B22 LE' + string(point3[1]+a)+ $
      ' AND B23 GE'+ string(point3[2]-a)+ ' and B23 LE' + string(point3[2]+a)+ $
      ' AND B24 GE'+ string(point3[3]-a)+ ' and B24 LE' + string(point3[3]+a)+ $
      ' AND B25 GE'+ string(point3[4]-a)+ ' and B25 LE' + string(point3[4]+a)+ $
      ' AND B26 GE'+ string(point3[5]-a)+ ' and B26 LE' + string(point3[5]+a)+ $
      ' AND B27 GE'+ string(point3[6]-b)+ ' and B27 LE' + string(point3[6]+b)+ $
      ' AND B28 GE'+ string(point3[7]-b)+ ' and B28 LE' + string(point3[7]+b)+ $
      ' AND B29 GE'+ string(point3[8]-a)+ ' and B29 LE' + string(point3[8]+a)+ $
      ' AND B30 GE'+ string(point3[9]-a)+ ' and B30 LE' + string(point3[9]+a)+ ')*1'
      
;      exp = '(B1 GE'+ string(point3[0]-a)+ ' and B1 LE' + string(point3[0]+a)+ $
;      ' AND B2 GE'+ string(point3[1]-a)+ ' and B2 LE' + string(point3[1]+a)+ $
;      ' AND B3 GE'+ string(point3[2]-a)+ ' and B3 LE' + string(point3[2]+a)+ $
;      ' AND B4 GE'+ string(point3[3]-a)+ ' and B4 LE' + string(point3[3]+a)+ $
;      ' AND B5 GE'+ string(point3[4]-a)+ ' and B5 LE' + string(point3[4]+a)+ $
;      ' AND B6 GE'+ string(point3[5]-a)+ ' and B6 LE' + string(point3[5]+a)+ $
;      ' AND B7 GE'+ string(point3[6]-b)+ ' and B7 LE' + string(point3[6]+b)+ $
;      ' AND B8 GE'+ string(point3[7]-b)+ ' and B8 LE' + string(point3[7]+b)+ $
;      ' AND B9 GE'+ string(point3[8]-a)+ ' and B9 LE' + string(point3[8]+a)+ $
;      ' AND B10 GE'+ string(point3[9]-a)+ ' and B10 LE' + string(point3[9]+a)+ ')*1'
;      
;      mathband_fid=INTARR(nb)+in_fid3
;      pos = INDGEN(nb)

   

    mathband_fid=[INTARR(nb)+in_fid1, INTARR(nb)+in_fid2, INTARR(nb)+in_fid3]
    pos = [INDGEN(nb), INDGEN(nb), INDGEN(nb)]
    b_name = 'possible location ' + STRING(FILE_BASENAME(in_list1[i],'.bsq'))

    out_file = out_folder + '\' + FILE_BASENAME(in_list1[i],'.bsq') + '_point3_' + '.bsq'

    ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dimsin, exp=exp, out_bname=b_name ,out_name=out_file, r_fid=out_fid, /IN_MEMORY

    out_fid_arr[i]=out_fid

  ENDFOR


  pos2 = LONARR(N_ELEMENTS(out_fid_arr))*0
  dims3 = LONARR(5,N_ELEMENTS(out_fid_arr))
  FOR m=0, N_ELEMENTS(out_fid_arr)-1 DO dims3[*,m]=dimsin

  out_file = out_folder + '\' + 'Multi-temp_Point' + STRTRIM(STRING(point_no),1) + '.bsq'

  bnames = ['20x20','25x25','30x30','35x35','40x40','45x45','50x50']

  ENVI_DOIT, 'ENVI_LAYER_STACKING_DOIT', fid=out_fid_arr, r_fid=out_fid, pos=pos2, dims=dims3, $
    out_dt=4, out_ps=pixel_size, out_proj=projection, out_bname = bnames, out_name=out_file


END

