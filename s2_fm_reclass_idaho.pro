
;+
; :Description: 
;
; INPUT:  
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 17 July 2017
;-

PRO S2_FM_reclass_Idaho


  t1 = SYSTIME()


  compile_opt STRICTARR
  ENVI, /restore_base_save_files
  ENVI_BATCH_INIT

  ;;;;;;;;;;;;;;;;;;;;;

  in_file = 'X:\projects\000044_IdahoForestryGroup\04_DataProducts\L2\ModelMap\Phase2\Forest_mask\with_validation\forest_mask.tif' ; in path

;  out_file = 'X:\projects\000044_IdahoForestryGroup\04_DataProducts\L2\ModelMap\Phase2\Forest_mask\with_validation\forest_mask_reclassV2.tif'
  out_file = 'X:\projects\000044_IdahoForestryGroup\04_DataProducts\L2\ModelMap\Phase2\Forest_mask\with_validation\forest_mask_reclassV2_binary.tif'
 
  ;;;;;;;;;;;;;;;;;;;;;

  ENVI_OPEN_FILE, in_file, r_fid=in_fid, /NO_REALIZE
  ENVI_FILE_QUERY, in_fid, ns=ns, nl=nl, data_type=data_type, dims=dims, bnames=bnames
  projection= ENVI_GET_PROJECTION(fid = in_fid, pixel_size=pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=in_fid)

  pos=[0]  
;  exp = '(B1 GE 2 AND B1 LE 6)*2 + (B1 GE 7)*B1 + (B1 LE 1)*B1'
;  exp = '(B1 GE 2 AND B1 LE 6)*2 + (B1 EQ 7)*B1 +(B1 EQ 10 OR B1 EQ 11)*10 + (B1 EQ 12 OR B1 EQ 13)*12 + (B1 LE 1)*B1'
  exp = '(B1 EQ 10 OR B1 EQ 11)*1'
  
  ENVI_DOIT, 'MATH_DOIT', fid=in_fid, R_FID=out_fid, pos=[0], dims=dims, exp=exp, out_bname='forest mask', out_name=out_file, /IN_MEMORY


  ENVI_OUTPUT_TO_EXTERNAL_FORMAT, DIMS=dims, FID=out_fid, OUT_BNAME='forest mask', OUT_NAME=out_file, POS=[0], /TIFF


  t2 = SYSTIME()                                                    ; end time
  print, 'start time: ' + t1                                        ; print start time
  print, 'end time: ' + t2                                          ; print end time

END