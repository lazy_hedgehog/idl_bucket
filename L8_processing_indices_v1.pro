
;+
; :Description: 
;
; :INPUT:  
;
; :DEPENDENCE: 
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :DATE: 26 February 2018
;-


  PRO L8_processing_indices_v1

  COMPILE_OPT IDL2
  e = ENVI(/HEADLESS) ; ()
  
  ;;;;;;;;;;;;;;;;;;;;;
    ;;; TOA 
    in_path = 'K:\personal\kat\TOTRAM\Todd\LC08_L1TP_202025_20160529_20170324_01_T1'
;    in_path = 'K:\personal\kat\TOTRAM\Todd\LC08_L1TP_203024_20160605_20170324_01_T1'
;    in_path = 'K:\personal\kat\TOTRAM\Todd\LC08_L1TP_203024_20161011_20170319_01_T1'
;    in_path = 'K:\personal\kat\TOTRAM\Todd\\LC08_L1TP_203025_20161128_20170317_01_T1'

     ;;; BOA

  ;;;;;;;;;;;;;;;;;;;;;

  print, in_path

  in_name = 'LC08*T1_stack.dat'

  in_list = FILE_SEARCH(in_path,in_name)        ; find the number of files to be processed
  base_name = FILE_BASENAME(in_list,'stack.dat')
  
  in_stack =e.OpenRaster(in_list[0])
  in_stack_fid = ENVIRasterToFID(in_stack)
  
  ;;; NDVI
  OutFile = in_path + '\' + base_name + 'NDVI.bsq'

  expression = 'FLOAT(B1 - B2) / FLOAT(B1 + B2)'
  math_fid = [in_stack_fid, in_stack_fid]
  pos = [4,3]

  ENVI_FILE_QUERY, in_stack_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; NMDI
  OutFile = in_path + '\' + base_name + 'NMDI.bsq'

  expression = 'FLOAT(B1 - (B2 - B3)) / FLOAT(B1 + (B2 - B3))'
  math_fid = [in_stack_fid, in_stack_fid, in_stack_fid]
  pos = [4,5,6]

  ENVI_FILE_QUERY, in_stack_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; NDII
  OutFile = in_path + '\' + base_name + 'NDII.bsq'

  expression = 'FLOAT(B1 - B2) / FLOAT(B1 + B2)'
  math_fid = [in_stack_fid, in_stack_fid]
  pos = [4,6]

  ENVI_FILE_QUERY, in_stack_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; NDWI
  OutFile = in_path + '\' + base_name + 'NDWI.bsq'

  expression = 'FLOAT(B1 - B2) / FLOAT(B1 + B2)'
  math_fid = [in_stack_fid, in_stack_fid]
  pos = [4,5]

  ENVI_FILE_QUERY, in_stack_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile


  ;;; EVI
  OutFile = in_path + '\' + base_name + 'EVI.bsq'

  expression = '2.5*FLOAT(B1 - B2) / (FLOAT(B1 + 6*B2 - 7.5*B3 +10000))'
  math_fid = [in_stack_fid, in_stack_fid, in_stack_fid]
  pos = [4,3,1]

  ENVI_FILE_QUERY, in_stack_fid, DIMS=dims
  ENVI_DOIT, 'MATH_DOIT', FID = math_fid, DIMS = dims, POS = pos, EXP = expression, OUT_NAME = OutFile

  ; Close the ENVI session
  e.Close


END

