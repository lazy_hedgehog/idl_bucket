;+
; :Description:
;   Script to filtes time-series with Savitzky-Golay filter -> application of already existing function on the time series of images (time dimain).
; 	Script uses POLY_SMOOTH.PRO function from http://idlastro.gsfc.nasa.gov/ftp/pro/math/poly_smooth.pro
;
;
; :AUTHOR: Katarzyna Ewa Lewińska
; :YEAR: 2017
;
;       !!! THE SCRIPT IS MEMORY DEMANDING!!!
; WORKS WELL FOR MODIS TS (11 YEARS) DATA FOR SOUTH TYROL
;
;-

PRO sav_gol_filter_Rezatec

    COMPILE_OPT STRICTARR
        ENVI, /restore_base_save_files
    ENVI_BATCH_INIT

;; define the input and potput file. either putting a direct link
;in_file = 'G:\Base_Data\NDVI_UTM\NDVItimeseries01-13_DEG\NDVItimeseries01-13_DEG_INTbsq.bsq'
;out_file = 'G:\Base_Data\NDVI_UTM\NDVItimeseries01-13_DEG\NDVItimeseries01-13_DEG_INT_SG.bsq

; or selecting file through the ENVI widget
if N_ELEMENTS(in_file) EQ (0) THEN in_file = ENVI_PICKFILE()
if N_ELEMENTS(out_file) EQ (0) THEN out_file = ENVI_PICKFILE()

; parameters to be passed to the filtering functions (For more details please see the poly_smooth. pro function)
width = 5
degree = 2
nle = 2
nr = 2
order = 0

; open time series file and get information on it
ENVI_OPEN_FILE, in_file, r_fid = in_fid, /NO_REALIZE
ENVI_FILE_QUERY, in_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims, bnames=bnames
projection = ENVI_GET_PROJECTION(fid = in_fid, pixel_size = pixel_size) 
map_info = ENVI_GET_MAP_INFO(fid=in_fid)

; prepare arrays where each row is one image 
matrix = FINDGEN(ns*nl, nb)
matrix2 = FINDGEN(ns*nl, nb)

; fit the image into the array defined above
FOR i = 0, nb-1 DO BEGIN

band = ENVI_GET_DATA(fid=in_fid, dims=dims, pos=[i])
vector_band = REFORM(band, ns*nl)

matrix[*, i] = vector_band

ENDFOR

;data = REFORM(matrix, ns, nl, nb )

; for each pixel in time (collumn) apply the filter
FOR n = 0L, ns*nl-1L DO BEGIN

data = matrix[n,*]
data = TRANSPOSE(data)
filtered_data = poly_smooth( data, width, DEGREE=degree, NLEFT=[0,nle], NRIGHT=[0,nr], DERIV_ORDER=order) ;, COEFF = 
data = TRANSPOSE(data)

; fid smoothed data into the new array
matrix2[n,*] = filtered_data

ENDFOR

fid_mat = LONARR(nb)

; reform array rows again into the images and prepare parameters for layer-stacking (in memory)
FOR p = 0, nb-1 DO BEGIN
bandvec = matrix2[*,p]
band_band = REFORM(bandvec, ns, nl)

envi_write_envi_file, band_band, r_fid = r_fid, data_type=data_type, nb=nb1, nl=nl, ns=ns, map_info=map_info, /IN_MEMORY; , out_name=out_file

fid_mat[p] = r_fid

ENDFOR

   pos3 = LINDGEN(nb) & pos3[*]=0L ; was 0 for NDVI
   dims3 = LONARR(5,nb)
   FOR l=0,nb-1 DO dims3[*,l]=[-1,0,ns-1,0,nl-1]

; write out smoothed data
ENVI_DOIT, 'ENVI_LAYER_STACKING_DOIT', fid=fid_mat, r_fid=r_fid2, pos=pos3, dims=dims3, out_dt=data_type, out_ps=pixel_size, out_proj=projection, out_name=out_file, out_bname = bnames ;, /IN_MEMORY

print, 'DONE'

END