
PRO S2_ndwi_B3_B8

;FUNCTION S2_calculate_ndwi_B3_B8, in_dir, layerstack_fid, ndwi_B3_B8_fid ; out_ndwi_B8_B11 ;,
  ;FUNCTION S2_calculate_ndwi_B3_b8, ly_stack, ndwi_B3_B8_fid
  ;
  ;;;Katarzyna_Ewa_Lewinska

  ;;; FUNCTION CALCULATES NDWI USING SENTINEL-2 BAND 3 AND 8.
  ;;; INPUT FILE IS A LAYERSTACK OF SENTINEL-2 BANDS: B2, B3, B4, B5, B6, B7, B8, B8A, B11 and B12
  ;;; (IN THIS ORDER) DERIVED FOR EXAMPLE USING THE S2-layerstack_10m.pro FUNCTION
  ;;; !!! SINCE THE NDWI USES 1.24µm bandwith, it is impossible to calculate the exact NDWI using S2 data. !!!


  ly_stack = 'Z:\S2_S\tiles\T33VWJ\T33VWJ_20170602_layerstack.bsq'
  
  out_ndwi_B3_B8 = FILE_DIRNAME(ly_stack)+'\'+ FILE_BASENAME(ly_stack,'layerstack.bsq')+ 'ndwi_B3_B8.bsq'

  print, 'calculate NDWI (band 3 and 8)'


  COMPILE_OPT STRICTARR
      ENVI, /restore_base_save_files
  ENVI_BATCH_INIT

  ;;;OPEN INPUT FILE
    ENVI_OPEN_FILE, ly_stack, r_fid = layerstack_fid, /NO_REALIZE
  ENVI_FILE_QUERY, layerstack_fid, ns=ns, nl=nl, nb=nb, data_type=data_type, dims=dims, bnames=bnames
  projection = ENVI_GET_PROJECTION(fid = layerstack_fid, pixel_size = pixel_size)
  map_info = ENVI_GET_MAP_INFO(fid=layerstack_fid)

  pos = [1,6]; B3 and 8 respectively
  exp = 'FLOAT(FLOAT(b1)-FLOAT(b2))/FLOAT(FLOAT(b1)+FLOAT(b2))'
  mathband_fid=[layerstack_fid, layerstack_fid]

  b_name = 'NDWI: (B3-B8)/(B3+B8)'

  ENVI_DOIT, 'math_doit', fid=mathband_fid, pos=pos, dims=dims, exp=exp, out_bname=b_name ,out_name=out_ndwi_B3_B8, r_fid=ndwi_B3_B8_fid;, /IN_MEMORY

;  RETURN, ndwi_B3_B8_fid ; out_ndwi_B3_B8


END